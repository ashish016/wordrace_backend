# Steps to run backend
pip install -r requirements.txt

## create a .env file in root with following things
Example



- SECRET_KEY='
- DB_NAME=Word_Race
- DB_PORT=3306
- DB_USERNAME=xyz1234
- DB_PASSWORD=12345678
- DB_HOST=127.0.0.1
- ALLOWED_HOSTS=localhost,127.0.0.1,*
- ITEMS_PER_PAGE=10


## run the manage.py file
python manage.py runserver
